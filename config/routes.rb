Rails.application.routes.draw do


  devise_for :admins
resources :requests
resources :pianos
resources :contrabasses
resources :electric_guitars
resources :guitars
resources :pedals
resources :colors
resources :tipos
resources :captations
resources :materials
resources :cordas
resources :tarraxas
resources :bracos
resources :modelos

devise_for :users
root 'welcome#index'

end
