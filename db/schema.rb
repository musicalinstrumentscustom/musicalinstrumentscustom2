# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_09_022103) do

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "bracos", force: :cascade do |t|
    t.integer "qtde"
    t.float "preco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "captations", force: :cascade do |t|
    t.string "nome"
    t.float "preco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "colors", force: :cascade do |t|
    t.string "nome"
    t.float "preco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contrabasses", force: :cascade do |t|
    t.integer "modelo_id"
    t.integer "braco_id"
    t.integer "tarraxa_id"
    t.integer "corda_id"
    t.integer "captation_id"
    t.float "preco_total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["braco_id"], name: "index_contrabasses_on_braco_id"
    t.index ["captation_id"], name: "index_contrabasses_on_captation_id"
    t.index ["corda_id"], name: "index_contrabasses_on_corda_id"
    t.index ["modelo_id"], name: "index_contrabasses_on_modelo_id"
    t.index ["tarraxa_id"], name: "index_contrabasses_on_tarraxa_id"
  end

  create_table "cordas", force: :cascade do |t|
    t.integer "qtde"
    t.string "instrumento"
    t.float "preco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "electric_guitars", force: :cascade do |t|
    t.integer "modelo_id"
    t.integer "braco_id"
    t.integer "tarraxa_id"
    t.integer "corda_id"
    t.integer "material_id"
    t.float "preco_total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["braco_id"], name: "index_electric_guitars_on_braco_id"
    t.index ["corda_id"], name: "index_electric_guitars_on_corda_id"
    t.index ["material_id"], name: "index_electric_guitars_on_material_id"
    t.index ["modelo_id"], name: "index_electric_guitars_on_modelo_id"
    t.index ["tarraxa_id"], name: "index_electric_guitars_on_tarraxa_id"
  end

  create_table "guitars", force: :cascade do |t|
    t.integer "modelo_id"
    t.integer "braco_id"
    t.integer "tarraxa_id"
    t.integer "corda_id"
    t.float "preco_total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["braco_id"], name: "index_guitars_on_braco_id"
    t.index ["corda_id"], name: "index_guitars_on_corda_id"
    t.index ["modelo_id"], name: "index_guitars_on_modelo_id"
    t.index ["tarraxa_id"], name: "index_guitars_on_tarraxa_id"
  end

  create_table "materials", force: :cascade do |t|
    t.string "nome"
    t.float "preco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "modelos", force: :cascade do |t|
    t.string "nome"
    t.string "instrumento"
    t.float "preco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pedals", force: :cascade do |t|
    t.string "nome"
    t.float "preco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pianos", force: :cascade do |t|
    t.integer "tipo_id"
    t.integer "color_id"
    t.integer "pedal_id"
    t.float "preco_total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["color_id"], name: "index_pianos_on_color_id"
    t.index ["pedal_id"], name: "index_pianos_on_pedal_id"
    t.index ["tipo_id"], name: "index_pianos_on_tipo_id"
  end

  create_table "requests", force: :cascade do |t|
    t.integer "guitar_id"
    t.integer "electric_guitar_id"
    t.integer "contrabass_id"
    t.integer "piano_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contrabass_id"], name: "index_requests_on_contrabass_id"
    t.index ["electric_guitar_id"], name: "index_requests_on_electric_guitar_id"
    t.index ["guitar_id"], name: "index_requests_on_guitar_id"
    t.index ["piano_id"], name: "index_requests_on_piano_id"
    t.index ["user_id"], name: "index_requests_on_user_id"
  end

  create_table "tarraxas", force: :cascade do |t|
    t.string "nome"
    t.string "instrumento"
    t.float "preco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipos", force: :cascade do |t|
    t.string "nome"
    t.float "preco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
