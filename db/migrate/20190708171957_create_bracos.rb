class CreateBracos < ActiveRecord::Migration[5.2]
  def change
    create_table :bracos do |t|
      t.integer :qtde
      t.float :preco

      t.timestamps
    end
  end
end
