class CreateContrabasses < ActiveRecord::Migration[5.2]
  def change
    create_table :contrabasses do |t|
      t.references :modelo, foreign_key: true
      t.references :braco, foreign_key: true
      t.references :tarraxa, foreign_key: true
      t.references :corda, foreign_key: true
      t.references :captation, foreign_key: true
      t.float :preco_total

      t.timestamps
    end
  end
end
