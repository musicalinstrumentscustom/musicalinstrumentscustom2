class CreateRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :requests do |t|
      t.references :guitar, foreign_key: true
      t.references :electric_guitar, foreign_key: true
      t.references :contrabass, foreign_key: true
      t.references :piano, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
