class CreateCaptations < ActiveRecord::Migration[5.2]
  def change
    create_table :captations do |t|
      t.string :nome
      t.float :preco

      t.timestamps
    end
  end
end
