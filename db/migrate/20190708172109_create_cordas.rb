class CreateCordas < ActiveRecord::Migration[5.2]
  def change
    create_table :cordas do |t|
      t.integer :qtde
      t.string :instrumento
      t.float :preco

      t.timestamps
    end
  end
end
