class CreateTarraxas < ActiveRecord::Migration[5.2]
  def change
    create_table :tarraxas do |t|
      t.string :nome
      t.string :instrumento
      t.float :preco

      t.timestamps
    end
  end
end
