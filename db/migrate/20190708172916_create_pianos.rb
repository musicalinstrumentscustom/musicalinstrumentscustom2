class CreatePianos < ActiveRecord::Migration[5.2]
  def change
    create_table :pianos do |t|
      t.references :tipo, foreign_key: true
      t.references :color, foreign_key: true
      t.references :pedal, foreign_key: true
      t.float :preco_total

      t.timestamps
    end
  end
end
