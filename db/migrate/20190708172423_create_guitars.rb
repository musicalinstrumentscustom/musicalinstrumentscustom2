class CreateGuitars < ActiveRecord::Migration[5.2]
  def change
    create_table :guitars do |t|
      t.references :modelo, foreign_key: true
      t.references :braco, foreign_key: true
      t.references :tarraxa, foreign_key: true
      t.references :corda, foreign_key: true
      t.float :preco_total

      t.timestamps
    end
  end
end
