class Contrabass < ApplicationRecord
  belongs_to :modelo
  belongs_to :braco
  belongs_to :tarraxa
  belongs_to :corda
  belongs_to :captation
end
