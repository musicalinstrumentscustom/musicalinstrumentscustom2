class Request < ApplicationRecord
  belongs_to :guitar
  belongs_to :electric_guitar
  belongs_to :contrabass
  belongs_to :piano
  belongs_to :user
end
