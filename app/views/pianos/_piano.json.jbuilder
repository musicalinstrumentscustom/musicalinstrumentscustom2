json.extract! piano, :id, :tipo_id, :color_id, :pedal_id, :preco_total, :created_at, :updated_at
json.url piano_url(piano, format: :json)
