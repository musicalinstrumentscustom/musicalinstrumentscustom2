json.extract! contrabass, :id, :modelo_id, :braco_id, :tarraxa_id, :corda_id, :captation_id, :preco_total, :created_at, :updated_at
json.url contrabass_url(contrabass, format: :json)
