json.extract! guitar, :id, :modelo_id, :braco_id, :tarraxa_id, :corda_id, :preco_total, :created_at, :updated_at
json.url guitar_url(guitar, format: :json)
