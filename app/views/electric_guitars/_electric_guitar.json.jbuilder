json.extract! electric_guitar, :id, :modelo_id, :braco_id, :tarraxa_id, :corda_id, :material_id, :preco_total, :created_at, :updated_at
json.url electric_guitar_url(electric_guitar, format: :json)
