json.extract! request, :id, :guitar_id, :electric_guitar_id, :contrabass_id, :piano_id, :user_id, :created_at, :updated_at
json.url request_url(request, format: :json)
