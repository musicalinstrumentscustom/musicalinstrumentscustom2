class ContrabassesController < ApplicationController
  before_action :set_contrabass, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  # GET /contrabasses
  # GET /contrabasses.json
  def index
    @contrabasses = Contrabass.all
  end

  # GET /contrabasses/1
  # GET /contrabasses/1.json
  def show
  end

  # GET /contrabasses/new
  def new
    @contrabass = Contrabass.new
  end

  # GET /contrabasses/1/edit
  def edit
  end

  # POST /contrabasses
  # POST /contrabasses.json
  def create
    @contrabass = Contrabass.new(contrabass_params)

    respond_to do |format|
      if @contrabass.save
        sum
        newrequest
        format.html { redirect_to @contrabass, notice: 'Pedido Feito com Sucesso.' }
        format.json { render :show, status: :created, location: @contrabass }
      else
        format.html { render :new }
        format.json { render json: @contrabass.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contrabasses/1
  # PATCH/PUT /contrabasses/1.json
  def update
    respond_to do |format|
      if @contrabass.update(contrabass_params)
        sum
        format.html { redirect_to @contrabass, notice: 'Pedido Atualizado com Sucesso.' }
        format.json { render :show, status: :ok, location: @contrabass }
      else
        format.html { render :edit }
        format.json { render json: @contrabass.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contrabasses/1
  # DELETE /contrabasses/1.json
  def destroy
    @contrabass.destroy
    respond_to do |format|
      format.html { redirect_to contrabasses_url, notice: 'Pedido Cancelado com Sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contrabass
      @contrabass = Contrabass.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contrabass_params
      params.require(:contrabass).permit(:modelo_id, :braco_id, :tarraxa_id, :corda_id, :captation_id, :preco_total)
    end

    def sum
      @contrabass.preco_total = @contrabass.modelo.preco + @contrabass.braco.preco + @contrabass.tarraxa.preco + @contrabass.corda.preco + @contrabass.captation.preco
      @contrabass.save
    end

    def newrequest
      @request = Request.new
      @request.contrabass_id = @contrabass.id
      @request.user_id = current_user.id
      @request.save(validate: false)
    end
end
