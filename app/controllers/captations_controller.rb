class CaptationsController < ApplicationController
  before_action :set_captation, only: [:show, :edit, :update, :destroy]
  
  # GET /captations
  # GET /captations.json
  def index
    @captations = Captation.all
  end

  # GET /captations/1
  # GET /captations/1.json
  def show
  end

  # GET /captations/new
  def new
    @captation = Captation.new
  end

  # GET /captations/1/edit
  def edit
  end

  # POST /captations
  # POST /captations.json
  def create
    @captation = Captation.new(captation_params)

    respond_to do |format|
      if @captation.save
        format.html { redirect_to @captation, notice: 'Captation was successfully created.' }
        format.json { render :show, status: :created, location: @captation }
      else
        format.html { render :new }
        format.json { render json: @captation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /captations/1
  # PATCH/PUT /captations/1.json
  def update
    respond_to do |format|
      if @captation.update(captation_params)
        format.html { redirect_to @captation, notice: 'Captation was successfully updated.' }
        format.json { render :show, status: :ok, location: @captation }
      else
        format.html { render :edit }
        format.json { render json: @captation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /captations/1
  # DELETE /captations/1.json
  def destroy
    @captation.destroy
    respond_to do |format|
      format.html { redirect_to captations_url, notice: 'Captation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_captation
      @captation = Captation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def captation_params
      params.require(:captation).permit(:nome, :preco)
    end
end
