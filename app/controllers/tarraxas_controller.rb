class TarraxasController < ApplicationController
  before_action :set_tarraxa, only: [:show, :edit, :update, :destroy]

  # GET /tarraxas
  # GET /tarraxas.json
  def index
    @tarraxas = Tarraxa.all
  end

  # GET /tarraxas/1
  # GET /tarraxas/1.json
  def show
  end

  # GET /tarraxas/new
  def new
    @tarraxa = Tarraxa.new
  end

  # GET /tarraxas/1/edit
  def edit
  end

  # POST /tarraxas
  # POST /tarraxas.json
  def create
    @tarraxa = Tarraxa.new(tarraxa_params)

    respond_to do |format|
      if @tarraxa.save
        format.html { redirect_to @tarraxa, notice: 'Tarraxa was successfully created.' }
        format.json { render :show, status: :created, location: @tarraxa }
      else
        format.html { render :new }
        format.json { render json: @tarraxa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tarraxas/1
  # PATCH/PUT /tarraxas/1.json
  def update
    respond_to do |format|
      if @tarraxa.update(tarraxa_params)
        format.html { redirect_to @tarraxa, notice: 'Tarraxa was successfully updated.' }
        format.json { render :show, status: :ok, location: @tarraxa }
      else
        format.html { render :edit }
        format.json { render json: @tarraxa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tarraxas/1
  # DELETE /tarraxas/1.json
  def destroy
    @tarraxa.destroy
    respond_to do |format|
      format.html { redirect_to tarraxas_url, notice: 'Tarraxa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tarraxa
      @tarraxa = Tarraxa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tarraxa_params
      params.require(:tarraxa).permit(:nome, :instrumento, :preco)
    end
end
