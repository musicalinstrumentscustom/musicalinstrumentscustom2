class CordasController < ApplicationController
  before_action :set_corda, only: [:show, :edit, :update, :destroy]

  # GET /cordas
  # GET /cordas.json
  def index
    @cordas = Corda.all
  end

  # GET /cordas/1
  # GET /cordas/1.json
  def show
  end

  # GET /cordas/new
  def new
    @corda = Corda.new
  end

  # GET /cordas/1/edit
  def edit
  end

  # POST /cordas
  # POST /cordas.json
  def create
    @corda = Corda.new(corda_params)

    respond_to do |format|
      if @corda.save
        format.html { redirect_to @corda, notice: 'Corda was successfully created.' }
        format.json { render :show, status: :created, location: @corda }
      else
        format.html { render :new }
        format.json { render json: @corda.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cordas/1
  # PATCH/PUT /cordas/1.json
  def update
    respond_to do |format|
      if @corda.update(corda_params)
        format.html { redirect_to @corda, notice: 'Corda was successfully updated.' }
        format.json { render :show, status: :ok, location: @corda }
      else
        format.html { render :edit }
        format.json { render json: @corda.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cordas/1
  # DELETE /cordas/1.json
  def destroy
    @corda.destroy
    respond_to do |format|
      format.html { redirect_to cordas_url, notice: 'Corda was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_corda
      @corda = Corda.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def corda_params
      params.require(:corda).permit(:qtde, :instrumento, :preco)
    end
end
