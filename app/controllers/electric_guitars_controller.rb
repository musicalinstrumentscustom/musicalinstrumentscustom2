class ElectricGuitarsController < ApplicationController
  before_action :set_electric_guitar, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  # GET /electric_guitars
  # GET /electric_guitars.json
  def index
    @electric_guitars = ElectricGuitar.all
  end

  # GET /electric_guitars/1
  # GET /electric_guitars/1.json
  def show
  end

  # GET /electric_guitars/new
  def new
    @electric_guitar = ElectricGuitar.new
  end

  # GET /electric_guitars/1/edit
  def edit
  end

  # POST /electric_guitars
  # POST /electric_guitars.json
  def create
    @electric_guitar = ElectricGuitar.new(electric_guitar_params)

    respond_to do |format|
      if @electric_guitar.save
        sum
        newrequest
        format.html { redirect_to @electric_guitar, notice: 'Pedido Feito com Sucesso.' }
        format.json { render :show, status: :created, location: @electric_guitar }
      else
        format.html { render :new }
        format.json { render json: @electric_guitar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /electric_guitars/1
  # PATCH/PUT /electric_guitars/1.json
  def update
    respond_to do |format|
      if @electric_guitar.update(electric_guitar_params)
        sum
        format.html { redirect_to @electric_guitar, notice: 'Pedido Atualizado com Sucesso.' }
        format.json { render :show, status: :ok, location: @electric_guitar }
      else
        format.html { render :edit }
        format.json { render json: @electric_guitar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /electric_guitars/1
  # DELETE /electric_guitars/1.json
  def destroy
    @electric_guitar.destroy
    respond_to do |format|
      format.html { redirect_to electric_guitars_url, notice: 'Pedido Cancelado com Sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_electric_guitar
      @electric_guitar = ElectricGuitar.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def electric_guitar_params
      params.require(:electric_guitar).permit(:modelo_id, :braco_id, :tarraxa_id, :corda_id, :material_id, :preco_total)
    end

    def sum
      @electric_guitar.preco_total = @electric_guitar.modelo.preco + @electric_guitar.braco.preco + @electric_guitar.tarraxa.preco + @electric_guitar.corda.preco + @electric_guitar.material.preco
      @electric_guitar.save
    end

    def newrequest
      @request = Request.new
      @request.electric_guitar_id = @electric_guitar.id
      @request.user_id = current_user.id
      @request.save(validate: false)
    end
end
