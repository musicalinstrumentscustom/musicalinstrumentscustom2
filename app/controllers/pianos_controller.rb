class PianosController < ApplicationController
  before_action :set_piano, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /pianos
  # GET /pianos.json
  def index
    @pianos = Piano.all
  end

  # GET /pianos/1
  # GET /pianos/1.json
  def show
  end

  # GET /pianos/new
  def new
    @piano = Piano.new
  end

  # GET /pianos/1/edit
  def edit
  end

  # POST /pianos
  # POST /pianos.json
  def create
    @piano = Piano.new(piano_params)

    respond_to do |format|
      if @piano.save
        sum
        newrequest
        format.html { redirect_to @piano, notice: 'Pedido Feito com Sucesso.' }
        format.json { render :show, status: :created, location: @piano }
      else
        format.html { render :new }
        format.json { render json: @piano.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pianos/1
  # PATCH/PUT /pianos/1.json
  def update
    respond_to do |format|
      if @piano.update(piano_params)
        sum
        format.html { redirect_to @piano, notice: 'Pedido Atualizado com Sucesso.' }
        format.json { render :show, status: :ok, location: @piano }
      else
        format.html { render :edit }
        format.json { render json: @piano.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pianos/1
  # DELETE /pianos/1.json
  def destroy
    @piano.destroy
    respond_to do |format|
      format.html { redirect_to pianos_url, notice: 'Pedido Cancelado com Sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_piano
      @piano = Piano.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def piano_params
      params.require(:piano).permit(:tipo_id, :color_id, :pedal_id, :preco_total)
    end

    def sum
      @piano.preco_total = @piano.tipo.preco + @piano.color.preco + @piano.pedal.preco
      @piano.save
    end

    def newrequest
      @request = Request.new
      @request.piano_id = @piano.id
      @request.user_id = current_user.id
      @request.save(validate: false)
    end
end
