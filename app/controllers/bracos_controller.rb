class BracosController < ApplicationController
  before_action :set_braco, only: [:show, :edit, :update, :destroy]
  
  # GET /bracos
  # GET /bracos.json
  def index
    @bracos = Braco.all
  end

  # GET /bracos/1
  # GET /bracos/1.json
  def show
  end

  # GET /bracos/new
  def new
    @braco = Braco.new
  end

  # GET /bracos/1/edit
  def edit
  end

  # POST /bracos
  # POST /bracos.json
  def create
    @braco = Braco.new(braco_params)

    respond_to do |format|
      if @braco.save
        format.html { redirect_to @braco, notice: 'Braco was successfully created.' }
        format.json { render :show, status: :created, location: @braco }
      else
        format.html { render :new }
        format.json { render json: @braco.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bracos/1
  # PATCH/PUT /bracos/1.json
  def update
    respond_to do |format|
      if @braco.update(braco_params)
        format.html { redirect_to @braco, notice: 'Braco was successfully updated.' }
        format.json { render :show, status: :ok, location: @braco }
      else
        format.html { render :edit }
        format.json { render json: @braco.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bracos/1
  # DELETE /bracos/1.json
  def destroy
    @braco.destroy
    respond_to do |format|
      format.html { redirect_to bracos_url, notice: 'Braco was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_braco
      @braco = Braco.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def braco_params
      params.require(:braco).permit(:qtde, :preco)
    end
end
