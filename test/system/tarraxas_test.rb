require "application_system_test_case"

class TarraxasTest < ApplicationSystemTestCase
  setup do
    @tarraxa = tarraxas(:one)
  end

  test "visiting the index" do
    visit tarraxas_url
    assert_selector "h1", text: "Tarraxas"
  end

  test "creating a Tarraxa" do
    visit tarraxas_url
    click_on "New Tarraxa"

    fill_in "Instrumento", with: @tarraxa.instrumento
    fill_in "Nome", with: @tarraxa.nome
    fill_in "Preco", with: @tarraxa.preco
    click_on "Create Tarraxa"

    assert_text "Tarraxa was successfully created"
    click_on "Back"
  end

  test "updating a Tarraxa" do
    visit tarraxas_url
    click_on "Edit", match: :first

    fill_in "Instrumento", with: @tarraxa.instrumento
    fill_in "Nome", with: @tarraxa.nome
    fill_in "Preco", with: @tarraxa.preco
    click_on "Update Tarraxa"

    assert_text "Tarraxa was successfully updated"
    click_on "Back"
  end

  test "destroying a Tarraxa" do
    visit tarraxas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tarraxa was successfully destroyed"
  end
end
