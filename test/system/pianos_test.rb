require "application_system_test_case"

class PianosTest < ApplicationSystemTestCase
  setup do
    @piano = pianos(:one)
  end

  test "visiting the index" do
    visit pianos_url
    assert_selector "h1", text: "Pianos"
  end

  test "creating a Piano" do
    visit pianos_url
    click_on "New Piano"

    fill_in "Color", with: @piano.color_id
    fill_in "Pedal", with: @piano.pedal_id
    fill_in "Preco total", with: @piano.preco_total
    fill_in "Tipo", with: @piano.tipo_id
    click_on "Create Piano"

    assert_text "Piano was successfully created"
    click_on "Back"
  end

  test "updating a Piano" do
    visit pianos_url
    click_on "Edit", match: :first

    fill_in "Color", with: @piano.color_id
    fill_in "Pedal", with: @piano.pedal_id
    fill_in "Preco total", with: @piano.preco_total
    fill_in "Tipo", with: @piano.tipo_id
    click_on "Update Piano"

    assert_text "Piano was successfully updated"
    click_on "Back"
  end

  test "destroying a Piano" do
    visit pianos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Piano was successfully destroyed"
  end
end
