require "application_system_test_case"

class ContrabassesTest < ApplicationSystemTestCase
  setup do
    @contrabass = contrabasses(:one)
  end

  test "visiting the index" do
    visit contrabasses_url
    assert_selector "h1", text: "Contrabasses"
  end

  test "creating a Contrabass" do
    visit contrabasses_url
    click_on "New Contrabass"

    fill_in "Braco", with: @contrabass.braco_id
    fill_in "Captation", with: @contrabass.captation_id
    fill_in "Corda", with: @contrabass.corda_id
    fill_in "Modelo", with: @contrabass.modelo_id
    fill_in "Preco total", with: @contrabass.preco_total
    fill_in "Tarraxa", with: @contrabass.tarraxa_id
    click_on "Create Contrabass"

    assert_text "Contrabass was successfully created"
    click_on "Back"
  end

  test "updating a Contrabass" do
    visit contrabasses_url
    click_on "Edit", match: :first

    fill_in "Braco", with: @contrabass.braco_id
    fill_in "Captation", with: @contrabass.captation_id
    fill_in "Corda", with: @contrabass.corda_id
    fill_in "Modelo", with: @contrabass.modelo_id
    fill_in "Preco total", with: @contrabass.preco_total
    fill_in "Tarraxa", with: @contrabass.tarraxa_id
    click_on "Update Contrabass"

    assert_text "Contrabass was successfully updated"
    click_on "Back"
  end

  test "destroying a Contrabass" do
    visit contrabasses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Contrabass was successfully destroyed"
  end
end
