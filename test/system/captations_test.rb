require "application_system_test_case"

class CaptationsTest < ApplicationSystemTestCase
  setup do
    @captation = captations(:one)
  end

  test "visiting the index" do
    visit captations_url
    assert_selector "h1", text: "Captations"
  end

  test "creating a Captation" do
    visit captations_url
    click_on "New Captation"

    fill_in "Nome", with: @captation.nome
    fill_in "Preco", with: @captation.preco
    click_on "Create Captation"

    assert_text "Captation was successfully created"
    click_on "Back"
  end

  test "updating a Captation" do
    visit captations_url
    click_on "Edit", match: :first

    fill_in "Nome", with: @captation.nome
    fill_in "Preco", with: @captation.preco
    click_on "Update Captation"

    assert_text "Captation was successfully updated"
    click_on "Back"
  end

  test "destroying a Captation" do
    visit captations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Captation was successfully destroyed"
  end
end
