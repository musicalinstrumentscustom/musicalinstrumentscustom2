require "application_system_test_case"

class CordasTest < ApplicationSystemTestCase
  setup do
    @corda = cordas(:one)
  end

  test "visiting the index" do
    visit cordas_url
    assert_selector "h1", text: "Cordas"
  end

  test "creating a Corda" do
    visit cordas_url
    click_on "New Corda"

    fill_in "Instrumento", with: @corda.instrumento
    fill_in "Preco", with: @corda.preco
    fill_in "Qtde", with: @corda.qtde
    click_on "Create Corda"

    assert_text "Corda was successfully created"
    click_on "Back"
  end

  test "updating a Corda" do
    visit cordas_url
    click_on "Edit", match: :first

    fill_in "Instrumento", with: @corda.instrumento
    fill_in "Preco", with: @corda.preco
    fill_in "Qtde", with: @corda.qtde
    click_on "Update Corda"

    assert_text "Corda was successfully updated"
    click_on "Back"
  end

  test "destroying a Corda" do
    visit cordas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Corda was successfully destroyed"
  end
end
