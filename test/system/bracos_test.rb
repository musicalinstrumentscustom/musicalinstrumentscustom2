require "application_system_test_case"

class BracosTest < ApplicationSystemTestCase
  setup do
    @braco = bracos(:one)
  end

  test "visiting the index" do
    visit bracos_url
    assert_selector "h1", text: "Bracos"
  end

  test "creating a Braco" do
    visit bracos_url
    click_on "New Braco"

    fill_in "Preco", with: @braco.preco
    fill_in "Qtde", with: @braco.qtde
    click_on "Create Braco"

    assert_text "Braco was successfully created"
    click_on "Back"
  end

  test "updating a Braco" do
    visit bracos_url
    click_on "Edit", match: :first

    fill_in "Preco", with: @braco.preco
    fill_in "Qtde", with: @braco.qtde
    click_on "Update Braco"

    assert_text "Braco was successfully updated"
    click_on "Back"
  end

  test "destroying a Braco" do
    visit bracos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Braco was successfully destroyed"
  end
end
