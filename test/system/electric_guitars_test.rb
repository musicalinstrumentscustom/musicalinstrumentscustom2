require "application_system_test_case"

class ElectricGuitarsTest < ApplicationSystemTestCase
  setup do
    @electric_guitar = electric_guitars(:one)
  end

  test "visiting the index" do
    visit electric_guitars_url
    assert_selector "h1", text: "Electric Guitars"
  end

  test "creating a Electric guitar" do
    visit electric_guitars_url
    click_on "New Electric Guitar"

    fill_in "Braco", with: @electric_guitar.braco_id
    fill_in "Corda", with: @electric_guitar.corda_id
    fill_in "Material", with: @electric_guitar.material_id
    fill_in "Modelo", with: @electric_guitar.modelo_id
    fill_in "Preco total", with: @electric_guitar.preco_total
    fill_in "Tarraxa", with: @electric_guitar.tarraxa_id
    click_on "Create Electric guitar"

    assert_text "Electric guitar was successfully created"
    click_on "Back"
  end

  test "updating a Electric guitar" do
    visit electric_guitars_url
    click_on "Edit", match: :first

    fill_in "Braco", with: @electric_guitar.braco_id
    fill_in "Corda", with: @electric_guitar.corda_id
    fill_in "Material", with: @electric_guitar.material_id
    fill_in "Modelo", with: @electric_guitar.modelo_id
    fill_in "Preco total", with: @electric_guitar.preco_total
    fill_in "Tarraxa", with: @electric_guitar.tarraxa_id
    click_on "Update Electric guitar"

    assert_text "Electric guitar was successfully updated"
    click_on "Back"
  end

  test "destroying a Electric guitar" do
    visit electric_guitars_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Electric guitar was successfully destroyed"
  end
end
