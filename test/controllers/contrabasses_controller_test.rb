require 'test_helper'

class ContrabassesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contrabass = contrabasses(:one)
  end

  test "should get index" do
    get contrabasses_url
    assert_response :success
  end

  test "should get new" do
    get new_contrabass_url
    assert_response :success
  end

  test "should create contrabass" do
    assert_difference('Contrabass.count') do
      post contrabasses_url, params: { contrabass: { braco_id: @contrabass.braco_id, captation_id: @contrabass.captation_id, corda_id: @contrabass.corda_id, modelo_id: @contrabass.modelo_id, preco_total: @contrabass.preco_total, tarraxa_id: @contrabass.tarraxa_id } }
    end

    assert_redirected_to contrabass_url(Contrabass.last)
  end

  test "should show contrabass" do
    get contrabass_url(@contrabass)
    assert_response :success
  end

  test "should get edit" do
    get edit_contrabass_url(@contrabass)
    assert_response :success
  end

  test "should update contrabass" do
    patch contrabass_url(@contrabass), params: { contrabass: { braco_id: @contrabass.braco_id, captation_id: @contrabass.captation_id, corda_id: @contrabass.corda_id, modelo_id: @contrabass.modelo_id, preco_total: @contrabass.preco_total, tarraxa_id: @contrabass.tarraxa_id } }
    assert_redirected_to contrabass_url(@contrabass)
  end

  test "should destroy contrabass" do
    assert_difference('Contrabass.count', -1) do
      delete contrabass_url(@contrabass)
    end

    assert_redirected_to contrabasses_url
  end
end
