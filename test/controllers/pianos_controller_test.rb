require 'test_helper'

class PianosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @piano = pianos(:one)
  end

  test "should get index" do
    get pianos_url
    assert_response :success
  end

  test "should get new" do
    get new_piano_url
    assert_response :success
  end

  test "should create piano" do
    assert_difference('Piano.count') do
      post pianos_url, params: { piano: { color_id: @piano.color_id, pedal_id: @piano.pedal_id, preco_total: @piano.preco_total, tipo_id: @piano.tipo_id } }
    end

    assert_redirected_to piano_url(Piano.last)
  end

  test "should show piano" do
    get piano_url(@piano)
    assert_response :success
  end

  test "should get edit" do
    get edit_piano_url(@piano)
    assert_response :success
  end

  test "should update piano" do
    patch piano_url(@piano), params: { piano: { color_id: @piano.color_id, pedal_id: @piano.pedal_id, preco_total: @piano.preco_total, tipo_id: @piano.tipo_id } }
    assert_redirected_to piano_url(@piano)
  end

  test "should destroy piano" do
    assert_difference('Piano.count', -1) do
      delete piano_url(@piano)
    end

    assert_redirected_to pianos_url
  end
end
