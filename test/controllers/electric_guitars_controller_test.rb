require 'test_helper'

class ElectricGuitarsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @electric_guitar = electric_guitars(:one)
  end

  test "should get index" do
    get electric_guitars_url
    assert_response :success
  end

  test "should get new" do
    get new_electric_guitar_url
    assert_response :success
  end

  test "should create electric_guitar" do
    assert_difference('ElectricGuitar.count') do
      post electric_guitars_url, params: { electric_guitar: { braco_id: @electric_guitar.braco_id, corda_id: @electric_guitar.corda_id, material_id: @electric_guitar.material_id, modelo_id: @electric_guitar.modelo_id, preco_total: @electric_guitar.preco_total, tarraxa_id: @electric_guitar.tarraxa_id } }
    end

    assert_redirected_to electric_guitar_url(ElectricGuitar.last)
  end

  test "should show electric_guitar" do
    get electric_guitar_url(@electric_guitar)
    assert_response :success
  end

  test "should get edit" do
    get edit_electric_guitar_url(@electric_guitar)
    assert_response :success
  end

  test "should update electric_guitar" do
    patch electric_guitar_url(@electric_guitar), params: { electric_guitar: { braco_id: @electric_guitar.braco_id, corda_id: @electric_guitar.corda_id, material_id: @electric_guitar.material_id, modelo_id: @electric_guitar.modelo_id, preco_total: @electric_guitar.preco_total, tarraxa_id: @electric_guitar.tarraxa_id } }
    assert_redirected_to electric_guitar_url(@electric_guitar)
  end

  test "should destroy electric_guitar" do
    assert_difference('ElectricGuitar.count', -1) do
      delete electric_guitar_url(@electric_guitar)
    end

    assert_redirected_to electric_guitars_url
  end
end
