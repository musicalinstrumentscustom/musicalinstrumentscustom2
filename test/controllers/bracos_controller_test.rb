require 'test_helper'

class BracosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @braco = bracos(:one)
  end

  test "should get index" do
    get bracos_url
    assert_response :success
  end

  test "should get new" do
    get new_braco_url
    assert_response :success
  end

  test "should create braco" do
    assert_difference('Braco.count') do
      post bracos_url, params: { braco: { preco: @braco.preco, qtde: @braco.qtde } }
    end

    assert_redirected_to braco_url(Braco.last)
  end

  test "should show braco" do
    get braco_url(@braco)
    assert_response :success
  end

  test "should get edit" do
    get edit_braco_url(@braco)
    assert_response :success
  end

  test "should update braco" do
    patch braco_url(@braco), params: { braco: { preco: @braco.preco, qtde: @braco.qtde } }
    assert_redirected_to braco_url(@braco)
  end

  test "should destroy braco" do
    assert_difference('Braco.count', -1) do
      delete braco_url(@braco)
    end

    assert_redirected_to bracos_url
  end
end
