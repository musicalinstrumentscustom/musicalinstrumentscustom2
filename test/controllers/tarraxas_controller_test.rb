require 'test_helper'

class TarraxasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tarraxa = tarraxas(:one)
  end

  test "should get index" do
    get tarraxas_url
    assert_response :success
  end

  test "should get new" do
    get new_tarraxa_url
    assert_response :success
  end

  test "should create tarraxa" do
    assert_difference('Tarraxa.count') do
      post tarraxas_url, params: { tarraxa: { instrumento: @tarraxa.instrumento, nome: @tarraxa.nome, preco: @tarraxa.preco } }
    end

    assert_redirected_to tarraxa_url(Tarraxa.last)
  end

  test "should show tarraxa" do
    get tarraxa_url(@tarraxa)
    assert_response :success
  end

  test "should get edit" do
    get edit_tarraxa_url(@tarraxa)
    assert_response :success
  end

  test "should update tarraxa" do
    patch tarraxa_url(@tarraxa), params: { tarraxa: { instrumento: @tarraxa.instrumento, nome: @tarraxa.nome, preco: @tarraxa.preco } }
    assert_redirected_to tarraxa_url(@tarraxa)
  end

  test "should destroy tarraxa" do
    assert_difference('Tarraxa.count', -1) do
      delete tarraxa_url(@tarraxa)
    end

    assert_redirected_to tarraxas_url
  end
end
