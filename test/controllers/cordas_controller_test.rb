require 'test_helper'

class CordasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @corda = cordas(:one)
  end

  test "should get index" do
    get cordas_url
    assert_response :success
  end

  test "should get new" do
    get new_corda_url
    assert_response :success
  end

  test "should create corda" do
    assert_difference('Corda.count') do
      post cordas_url, params: { corda: { instrumento: @corda.instrumento, preco: @corda.preco, qtde: @corda.qtde } }
    end

    assert_redirected_to corda_url(Corda.last)
  end

  test "should show corda" do
    get corda_url(@corda)
    assert_response :success
  end

  test "should get edit" do
    get edit_corda_url(@corda)
    assert_response :success
  end

  test "should update corda" do
    patch corda_url(@corda), params: { corda: { instrumento: @corda.instrumento, preco: @corda.preco, qtde: @corda.qtde } }
    assert_redirected_to corda_url(@corda)
  end

  test "should destroy corda" do
    assert_difference('Corda.count', -1) do
      delete corda_url(@corda)
    end

    assert_redirected_to cordas_url
  end
end
